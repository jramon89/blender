angular.module('blender.invited')
    .controller('invitedController', invitedController)
invitedController.$inject = ['$rootScope', 'listBlends'];

function invitedController($rootScope, listBlends) {
    var invited = this;
    invited.blender = listBlends.blends[listBlends.blend]
    $rootScope.backTitle = invited.blender;

}

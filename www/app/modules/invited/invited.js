angular.module('blender.invited', [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.invited', {
                url: '/invited',
                views: {
                    'mainContent': {
                        templateUrl: 'app/modules/invited/templates/invited.html',
                        controller: 'invitedController',
                        controllerAs:'invited'
                    }
                }
            })
    }])

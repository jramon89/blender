angular.module('blender.layout', [])
    .run(function($rootScope) {
        $rootScope.backTitle = 'Back';
    })
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login')
        $stateProvider
            .state('app', {
                url: '/app',
                templateUrl: 'app/modules/layout/templates/layout.html',
                controller:'layoutController as layout'
            })
    }])

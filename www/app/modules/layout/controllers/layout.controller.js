angular.module('blender.layout')
    .controller('layoutController', layoutController);
layoutController.$inject = ['$state', 'blend']

function layoutController($state, blend) {
    var layout = this;

    layout.logOut = function() {
        if (confirm('Are you sure close session?')) {
            blend.storage.remove('session');
            $state.go('login');
        }
    }
}

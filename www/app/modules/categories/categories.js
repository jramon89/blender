// angular.module('blender.categories',[
//  'blender.music',
//  'blender.movies',
//  'blender.sport'
// ])
angular.module('blender.categories', [])
    .config(['$stateProvider', '$ionicConfigProvider',function($stateProvider, $ionicConfigProvider) {
      
        $stateProvider
            .state('app.category', {
                url: '/category',
                views: {
                    mainContent: {
                        templateUrl: 'app/modules/categories/templates/categories.html',
                        controller: 'categoryController as category'
                    }
                }
            })
            .state('app.category.sport', {
                url: '/:id',
                views: {
                    'category': {
                        templateUrl: 'app/modules/categories/templates/list.html',
                        controller: 'categoryController as category'
                    }
                }
            })
            /* .state('app.music', {
                 url: '/music',
                 views: {
                     mainContent: {
                         templateUrl: 'app/modules/categories/templates/music.html',
                         controller: 'musicController as music'
                     }
                 }
             })
             .state('app.movies', {
                 url: '/movies',
                 views: {
                     mainContent: {
                         templateUrl: 'app/modules/categories/templates/movies.html',
                         controller: 'movieController as movie'
                     }
                 }
             })*/


        // $ionicConfigProvider.backButton.text('xxxxxxxxxxx');

    }])

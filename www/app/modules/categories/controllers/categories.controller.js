angular.module('blender.categories')
    .controller('categoryController', categoryController)
categoryController.$inject = ['$rootScope', '$ionicModal', '$state', '$stateParams', 'blenderCategories'];

function categoryController($rootScope, $ionicModal, $state, $stateParams, blenderCategories) {
    var category = this;
    category.list = blenderCategories[$stateParams.id];
    $rootScope.backTitle = blenderCategories[$stateParams.id];

    $ionicModal.fromTemplateUrl('app/common/templates/modal.addItemCategory.html', {
        //scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        category.modal = modal;
    });

    category.throwModal = function() {
        category.modal.show();

    }

}

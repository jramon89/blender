angular.module('blender.login', [])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/modules/login/templates/login.html',
                controller: 'loginController as login'
            })

    }])

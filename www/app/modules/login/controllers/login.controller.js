angular.module('blender.login')
    .controller('loginController', loginController);
loginController.$inject = ['$scope', '$filter', '$state', 'blenderUsers', 'blend'];

function loginController($scope, $filter, $state, blenderUsers, blend) {
    login = this;

    login.getUser = function(value) {
        var currentUser = $filter('filter')(blenderUsers, value.user)
        if (currentUser.length) {
            if (currentUser[0].access.user === value.user) {
                //localStorage.setItem('session', JSON.stringify(currentUser[0].info))
                blend.storage.set('session', currentUser[0].info);
                $state.go('app.settings');
            } else {
                alert('Error')
            }
        }

    }

}

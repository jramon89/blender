angular.module('blender.modules', [
    'blender.layout',
    'blender.login',
    'blender.home',
    'blender.settings',
    'blender.categories',
    'blender.invited',
    'blender.profile'

])

angular.module('blender.profile',[])
.config(['$stateProvider', function($stateProvider){
	$stateProvider
	.state('app.profile',{
		url:'/profile',
		views:{
			'mainContent':{
				templateUrl:'app/modules/profile/templates/profile.html',
				controller:'profileController as profile'
			}
		}
	})
}])
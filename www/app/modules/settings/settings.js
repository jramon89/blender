angular.module('blender.settings', [])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app.settings', {
                url: '/settings',
                views: {
                    mainContent: {
                        templateUrl: 'app/modules/settings/templates/settings.html',
                        controller: 'settingsController as settings'
                    }
                }

            })
    }])

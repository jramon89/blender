angular.module('blender.settings')
    .controller('settingsController', settingsController)
settingsController.$inject = ['$scope', 'blenderCategories', 'blend'];

function settingsController($scope, blenderCategories, blend) {
    var settings = this;
    console.log(blenderCategories)
    settings.categories = blenderCategories;
    $scope.$watch(function() {
        //settings.userLoged = JSON.parse(localStorage.getItem('session'));
        settings.userLoged = blend.storage.get('session');

    })
}

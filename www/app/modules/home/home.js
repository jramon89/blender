angular.module('blender.home', [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.home', {
                url: '/home',
                views: {
                    'mainContent': {
                        templateUrl: 'app/modules/home/templates/home.html',
                        controller: 'homeController as home',
                    }
                }
            })
        

    }])

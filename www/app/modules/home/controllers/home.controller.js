angular.module('blender.home')
    .controller('homeController', homeController)
homeController.$inject = ['$scope', '$location', '$state', '$ionicModal', 'listBlends', 'blend'];

function homeController($scope, $location, $state, $ionicModal, listBlends, blend) {
    var home = this;
    home.allBlends = listBlends.blends;
    $ionicModal.fromTemplateUrl('app/common/templates/modal.newBlender.html', {
        //scope: home,
        animation: 'slide-in-up'
    }).then(function(modal) {
        home.modal = modal;
        console.log(home.modal);
    });

    $scope.$watch(function() {
        home.userLoged = blend.storage.get('session');
    })

    home.openModal = function() {

        home.modal.show();
    }

    home.selectBlend = function(blend) {
            listBlends.blend = blend;
            $state.go('app.invited');

        }
        //console.log($location);

}

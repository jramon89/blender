angular.module('blender.services')
    .factory('blend', blend)

function blend() {
    var storage = {
        set: function(name, value) {
            if (typeof value === 'string') {
                localStorage.setItem(name, value);
            } else {
                localStorage.setItem(name, JSON.stringify(value));
            }
        },
        get: function(name) {
            var storage = localStorage.getItem(name);
            if (typeof storage === 'string') {
                return JSON.parse(storage);
            } else {
                return storage;
            }
        },
        remove: function(name) {
            localStorage.removeItem(name);
        }
    }

    return { storage: storage };
}

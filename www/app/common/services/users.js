angular.module('blender.services')
    .factory('blenderUsers', blenderUsers)

function blenderUsers() {
    var users = [{
            "info": {
                "id": "",
                "name": "Jose Ramon",
                "lastName": "Roblero Ruiz",
                "info":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit ame"
            },
            "access": {
                "user": "jramon89",
                "password": "webmaster_19",

            }
        },

        {
            "info": {
                "id": "",
                "name": "Miguel Angel",
                "lastName": "",
                "info":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit ame"
            },
            "access": {
                "user": "miguel123",
                "password": "miguel123"
            }
        },

        {
            "info": {
                "id": "",
                "name": "Jhonatan",
                "lastName": "",
                "info":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit ame"
            },
            "access": {
                "user": "jhon123",
                "password": "jhon123"
            }
        }
    ];


    return users;
}


// [{
//      "id": "",
//      "name": "Jose Ramon",
//      "lastName": "Roblero Ruiz",
//      "access": {
//          "user": "jramon89",
//          "password": "webmaster_19"
//      }
//  },

//  {
//      "id": "",
//      "name": "Miguel Angel",
//      "lastName": "",
//      "access": {
//          "user": "miguel123",
//          "password": "miguel123"
//      }
//  },

//  {
//      "id": "",
//      "name": "Jhonatan",
//      "lastName": "",
//      "access": {
//          "user": "jhon123",
//          "password": "jhon123"
//      }
//  }
// ]

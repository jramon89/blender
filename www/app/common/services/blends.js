angular.module('blender.services')
    .factory('listBlends', listBlends)

function listBlends() {
    var blends = [{
        "title": "Angularians",
        "userName": "José Ramón Roblero",
        "date": "22/04/2016",
        "hour": "14:00",
        "place": "Chill out piso 15",
        "blendType": "",
        "blendDescription": "xxxxxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxx xxxxxxxxx xxxxxxx",
        "numInvited": ""
    }, {
        "title": "Let's rest",
        "userName": "Jonathan Becerril",
        "date": "22/04/2016",
        "hour": "14:00",
        "place": "Chill out piso 16",
        "blendType": "",
        "blendDescription": "xxxxxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxx xxxxxxxxx xxxxxxx",
        "numInvited": ""
    }, {
        "title": "Fans to Game of Thrones",
        "userName": "Yadira Cisneros",
        "date": "22/04/2016",
        "hour": "14:00",
        "place": "Chill out piso 16",
        "blendType": "",
        "blendDescription": "xxxxxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxx xxxxxxxxx xxxxxxx",
        "numInvited": ""
    }];

    var blend = null;

    return {
        blends: blends,
        blend: blend
    };
}

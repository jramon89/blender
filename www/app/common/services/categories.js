angular.module('blender.services')
    .factory('blenderCategories', blenderUsers)

function blenderUsers() {


    var categories = {
        "music": {
            "title": "Music",
            "link": "music",
            "content": [{
                "name": "Adele"
            }, {
                "name": "Artick monkeys"
            }, {
                "name": "Audioslave"
            }]
        },
        "movies": {
            "title": "Movies",
            "link": "movies",
            "content": [{
                "name": "Batman vs Superman"
            }, {
                "name": "Civil Warr"
            }]
        },
        "sport": {
            "title": "Sport",
            "link": "sport",
            "content": []
        }
    }

    return categories;
}

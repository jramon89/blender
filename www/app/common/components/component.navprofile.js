angular.module('blender.barProfile', [])
    .directive('ionBarProfile', ionBarProfile);

function ionBarProfile() {
    var directive = {
        scope: {
            marginBottom: '@'
        },
        transclude: {
            'title': '?profileTitle',
            'content': '?profileContent'
        },
        restict: 'AE',
        templateUrl: 'app/common/components/templates/component.navprofile.html',
        link: link
    }
    return directive;

    function link(scope, element, attr) {
        var el = element[0];
        el.nextElementSibling.style.top = scope.marginBottom + 'px';
    }
}
